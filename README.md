
# Sequence-to-sequence workflows for NER

This is a small project where I will put together various steps involved in sequence-to-sequence workflow, from accessing the data to evaluating the output of a system. While performing the task, Intended learning outcomes are:  

- what are the most important data sets and tools for sequence-to-sequence processing 
- how the processing workflows changed from statistical to neural solutions
- the key steps in the NLP research
- how to write clear and meaningful project reports 

The tasks can be summarized in the following steps: 

- Download and pre-process the data: **CoNLL2003** 
- Establish the baseline performance with a pre-neural tool: **CRF model**
- Establish the baseline performance with a neural tool: **Transformer-based model**
- Analyse system outputs
- Propose improvements for pre-neural and neural models
- Evaluate the improvements
- Write a project report describing all the steps.



